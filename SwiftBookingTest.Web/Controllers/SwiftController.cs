﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SwiftBookingTest.Web.Models;

namespace SwiftBookingTest.Web.Controllers
{
    public class SwiftController : ApiController
    {
        //Todo: Should be refactored in a more meaningful naming; leaving as is for the test.
        // POST api/<controller>
        public IHttpActionResult Post(string address)
        {
            return Ok(Swift.BookDelivery(address));
        }
    }
}
