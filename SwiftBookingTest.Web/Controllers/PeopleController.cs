﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SwiftBookingTest.Web.Models;

namespace SwiftBookingTest.Web.Controllers
{
    public class PeopleController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Person> Get()
        {
            using (BookingDataContext db = new BookingDataContext())
            {
                return db.People.ToList();
            }
        }

        // GET api/<controller>/5
        public IHttpActionResult Get(int id)
        {
            using (BookingDataContext db = new BookingDataContext())
            {
                var result = db.People.Find(id);
                if (result == null) return NotFound();
                return Ok(result);
            }
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody]Person value)
        {
            if (value == null) return BadRequest("value can't be empty!");
            else if (!ModelState.IsValid) return new System.Web.Http.Results.InvalidModelStateResult(ModelState, this);
            using (BookingDataContext db = new BookingDataContext())
            {
                var result = db.People.Add(value);
                db.SaveChanges();
                return Ok(result);
            }
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}