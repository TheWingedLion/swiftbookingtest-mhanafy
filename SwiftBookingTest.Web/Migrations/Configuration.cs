namespace SwiftBookingTest.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using SwiftBookingTest.Web.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<SwiftBookingTest.Web.Models.BookingDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SwiftBookingTest.Web.Models.BookingDataContext context)
        {
            context.People.AddOrUpdate(p => p.Name, new Person { Name = "Mahmoud Hanafy", Address = "N/A", Phone = "0202020202" });
            context.SaveChanges();
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
