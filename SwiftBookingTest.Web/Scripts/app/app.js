﻿//Below commented config section was a trial to avoid sending OPTIONS header as part of the CORS handshake, since the server doesn't support this verb.
//After researching throughly, I decided to move this to the server side, then to be consumed by another WebApi.

angular.module('swiftbookingApp', [])
//.config(['$httpProvider', function ($httpProvider) {
//    //Reset headers to avoid OPTIONS request (aka preflight)
//    $httpProvider.defaults.headers.common = {};
//    $httpProvider.defaults.headers.post = {};
//    $httpProvider.defaults.headers.put = {};
//    $httpProvider.defaults.headers.patch = {};
//}])
.controller('PeopleListCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.people = [];

    $scope.getPeople = function () {
        var scope = this;
        $http.get('/api/People')
        .then(function (response) { $scope.people = response.data;});
    }

    $scope.addName = "";
    $scope.addPhone = "";
    $scope.addAddress = "";

    $scope.getPeople();

    $scope.insertPerson = function()
    {
        //Todo: Add some client side validation to avoid unnecessary round trips
        var person = {Name: $scope.addName, Phone: $scope.addPhone, Address: $scope.addAddress};
        var insertedPerson;
        $http({
            url: '/api/People/Post',
            method: 'POST',
            data: person,
            headers: { 'Content-Type': 'application/json' }
        }).then(
        //success callback
        function (response) {
            insertedPerson = response.data;
            $scope.message = 'Person Inserted!';
            $scope.people.push(insertedPerson);
            //Resetting fields to empty strings
            $scope.addName = "";
            $scope.addPhone = "";
            $scope.addAddress = "";
        },//error callback
        function (response, status, headers, config) {
            var msg = 'Please correct below errors and try again!\n';
            var state = response.data.ModelState;
            for (att in state)
            {
                msg += att + ': ' + state[att] + '; ';
            }
            //data.data.ModelState.forEach(function(item, index){msg+=item+'\n';});
            //$scope.message = msg;
            $scope.message = msg;
        });
        return insertedPerson;
    }

    $scope.message = "";


    /*Originally, I wrote this function to directly call the Swift API from the client, but I kept facing below error:
    {"message":"The requested resource does not support http method 'OPTIONS'."}
    After a through research, decided to do the call on the server side, and consume it indirectly by an API call.
    */

    //$scope.callSwift = function(address)
    //{
    //    console.log("add", address);
    //    var person = { Name: $scope.addName, Phone: $scope.addPhone, Address: $scope.addAddress };

    //    $http({
    //        url: 'https://app.getswift.co/api/v2/deliveries',
    //        method: 'POST',
    //        data: 
    //            {
    //            "apiKey": "3285db46-93d9-4c10-a708-c2795ae7872d",
    //            "booking": {
    //                "pickupDetail": { "address": "57 luscombe st, brunswick, melbourne" },
    //                "dropoffDetail": { "address": "Test address" }
    //            }
    //            }
    //        ,
    //        headers: { 'Content-Type': 'application/json' }
    //    }).then(
    //    //success callback
    //    function (response) {
    //        $scope.message = response.data;
    //        console.log("success", response.data);
    //    },//error callback
    //    function (response, status, headers, config) {
    //        $scope.message = response.data;
    //        console.log("failure", response);
    //    });
    //}

    //below uses an API wrapper to call swift API, avoiding the validation trip with unsupported verb:OPTIONS
    $scope.callSwift = function (address) {
        $http({
            url: '/api/Swift/Post?address='+address,
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        }).then(
        //success callback
        function (response) {
            $scope.message = response.data;
            console.log("success", response.data);
        },//error callback
        function (response, status, headers, config) {
            $scope.message = response.data;
            console.log("failure", response);
        });
    }
}]);

