﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace SwiftBookingTest.Web.Models
{

    public static class Swift
    {
        /*HttpClient is intended to be instantiated once and re-used throughout the life of an application.
        Especially in server applications, creating a new HttpClient instance for every request
        will exhaust the number of sockets available under heavy loads.This will result in SocketException errors.*/
        static HttpClient client = new HttpClient();
        const string ApiKey = "3285db46-93d9-4c10-a708-c2795ae7872d";
        const string pickupAddress = "105 collins st, 3000";

        static Swift()
        {
            client.BaseAddress = new Uri("https://app.getswift.co/api/v2/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public static string BookDelivery(string address)
        {
            HttpResponseMessage response = client.PostAsJsonAsync("deliveries", new
            {
                apiKey = ApiKey,
                booking = new
                {
                    pickupDetail = new { address = pickupAddress },
                    dropoffDetail = new { address = address }
                }
            }).Result;
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}