﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Models
{
    public class Person
    {
        public int PersonID { get; set; }
        [Required, MinLength(5), MaxLength(50)]
        public string Name { get; set; }
        [Required, Phone()]
        public string Phone { get; set; }
        [Required, MaxLength(255)]
        public string Address { get; set; }
    }

    public class BookingDataContext : DbContext
    {
        public BookingDataContext() : base("SwiftBooking") { }
        public DbSet<Person> People { get; set; }
    }
}